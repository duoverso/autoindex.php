<?php
//ini_set('display_errors', 1);
//ini_set('display_startup_errors', 1);
//error_reporting(E_ALL);

function dump($var) {
    echo('<pre>' . print_r($var, true) . '</pre>');
}

function dd($var) {
    die('<pre>' . print_r($var, true) . '</pre>');
}

// Config for current dir only (use autoindex.json for shared config for parent dir(s) & subdirs)
$cfg = [];

function loadCfgJson(string $path) {
    global $cfg;
    if (file_exists($path . '/autoindex.json')) {
        $json = json_decode(file_get_contents($path . '/autoindex.json'), true);
        if (isset($json['keep_parent_config']) && !$json['keep_parent_config']) {
            $cfg = [];
        }
        $cfg = array_merge($cfg, $json);
    }
}

$uri = urldecode($_SERVER['REQUEST_URI']);
// Get directory of requested URI (add any filename to URI if it's directory) relative to document root
$relPath = dirname(substr($uri, -1) == '/' ? $uri . '?' : $uri);
$absPath = $_SERVER['DOCUMENT_ROOT'] . $relPath;
loadCfgJson($_SERVER['DOCUMENT_ROOT']);

$breadcrumbs = explode('/', $relPath);
array_shift($breadcrumbs);
$breadcrumbsHtml = $breadcrumbsPath = '';

// Lookup from $_SERVER['DOCUMENT_ROOT'] to $relPath
foreach ($breadcrumbs as $num => $breadcrumb) {
    $breadcrumbsPath .= '/' . $breadcrumb;
    $breadcrumbsHtml .= '<a href="' . $breadcrumbsPath . '/">' . $breadcrumb . '</a> / ';
    loadCfgJson($_SERVER['DOCUMENT_ROOT'] . $breadcrumbsPath);
}

// Default config
if (!isset($cfg['ignore'])) {
    $cfg['ignore'] = ['index.php', 'autoindex.json'];
}
if (!isset($cfg['link_to_parent'])) {
    $cfg['link_to_parent'] = true;
}
if (!isset($cfg['dir_suffix'])) {
    $cfg['dir_suffix'] = '/'; // Add string (eg. slash) to each directory returned
}
if (!isset($cfg['dirs_only'])) {
    $cfg['dirs_only'] = false;
}
if (!isset($cfg['columns'])) {
    $cfg['columns'] = ['name', 'date', 'size'];
}
if (!isset($cfg['header'])) {
    $cfg['header'] = false; // ['Name', 'Date', 'Size'];
}
if (!isset($cfg['max_file_length'])) {
    $cfg['max_file_length'] = 70; // characters
}
if (!isset($cfg['date_format'])) {
    $cfg['date_format'] = 'd-M-Y H:i'; // see https://www.php.net/manual/en/datetime.format.php
}
if (!isset($cfg['exact_size'])) {
    $cfg['exact_size'] = true; // true for size in bytes, false for human readable with size_precision
}
if (!isset($cfg['size_precision'])) {
    $cfg['size_precision'] = 2; // decimal points
}
if (!isset($cfg['filler'])) {
    $cfg['filler'] = ' '; // String to fill gaps on start and end of columns
}
if (!isset($cfg['separator'])) {
    $cfg['separator'] = '    '; // (string) separator between columns
}
if (!isset($cfg['separator_on_start'])) {
    $cfg['separator_on_start'] = false;
}
if (!isset($cfg['separator_on_end'])) {
    $cfg['separator_on_end'] = false;
}
if (!isset($cfg['show_hidden'])) {
    $cfg['show_hidden'] = false;
}
if (!isset($cfg['style'])) {
    $cfg['style'] = false; // (string) CSS added to <html><head><style>...</style>
}

$columnKeys = array_flip($cfg['columns']);

$replacements = [
    '%path%' => $relPath,
    '%breadcrumbs%' => $breadcrumbsHtml,
    '%host%' => $_SERVER['HTTP_HOST'],
    '%port%' => $_SERVER['SERVER_PORT'],
];
if (!isset($cfg['title'])) {
    $cfg['title'] = $cfg['heading'] ?? 'Index of %path%'; // (string) title for <title> & <h1>
}
$title = strtr($cfg['title'], $replacements);
if (!isset($cfg['heading'])) {
    //$cfg['heading'] = $cfg['title'];
    $heading = $title;
} else {
    $heading = strtr($cfg['heading'], $replacements);
}

function hrSize($bytes, $precision = 2) {
    if ($bytes < 1024) {
        return $bytes . ' B ';
    } elseif ($bytes < (1024**2)) {
        return number_format($bytes / 1024, $precision) . ' kB';
    } elseif ($bytes < (1024**3)) {
        return number_format($bytes / (1024**2), $precision) . ' MB';
    } else {
        return number_format($bytes / (1024**3), $precision) . ' GB';
    }
}

function videoThumbnail(string $input, string $outputFile, string $skip = '00:15:00.00') {
    `ffmpeg -ss $skip -i "$inputFile" -vframes 1 "$outputFile"`;
}

/** Length of audio/video file/stream (in seconds) */
function avLength(string $file) {
    return (`mediainfo --Inform="General;%Duration%" "$file"` / 1e3); // ms
}


$items = [];

/**
 * Add associative item (eg. ['name' => 'index.html', 'size' => 14]) to $items (with items indexed by column number in $cfg['columns'] or $columnKeys respectively)
 */
function addItem(array $associativeItem) {
    global $items, $columnKeys;
    $newItem = [];
    foreach ($columnKeys as $key => $num) {
        if (isset($associativeItem[$key])) {
            $newItem[$num] = $associativeItem[$key];
        }
        // num of column is changing by cfg['columns'] so it should not be added directly
        /*elseif (isset($associativeItem[$num])) {
            $newItem[$num] = $associativeItem[$num];
        }*/
    }
    $items[] = $newItem;
}

if ($cfg['link_to_parent']) {
    addItem(['name' => '../', 'size' => '-']);
}

$maxFileLength = $maxSizeLength = 0;
foreach ($items as $item) {
    if (isset($columnKeys['name'])) {
        $maxFileLength = max(mb_strlen($item[$columnKeys['name']]), $maxFileLength);
    }
    if (isset($columnKeys['size'])) {
        $maxSizeLength = max(strlen($item[$columnKeys['size']]), $maxSizeLength);
    }
}

$glob = glob($absPath . '/*', $cfg['dirs_only'] ? GLOB_ONLYDIR : 0);
if ($cfg['show_hidden']) {
    $glob = array_merge(glob($absPath . '/.*', $cfg['dirs_only'] ? GLOB_ONLYDIR : 0), $glob);
}
foreach ($glob as $absName) {
    $name = basename($absName);
    if (in_array($name, $cfg['ignore'])) {
        continue;
    }
    if (is_dir($absName)) {
        $size = '-';
        $name .= ($cfg['dir_suffix'] ?? '');
    } else {
        $size = filesize($absName);
        if (!$cfg['exact_size']) {
            $size = hrSize($size, $cfg['size_precision']);
        }
    }
    addItem(['name' => $name, 'size' => $size]);

    $maxFileLength = max(mb_strlen($name), $maxFileLength);
    $maxSizeLength = max(strlen($size), $maxSizeLength);
}
if ($cfg['max_file_length'] ?? 0) {
    $maxFileLength = min($cfg['max_file_length'], $maxFileLength);
}
if ($cfg['header'] && ($num = array_search('size', $cfg['columns'])) !== false) {
    $maxSizeLength = max(strlen($cfg['header'][$num]), $maxSizeLength);
}

?>
<html>
<head>
    <title><?= $title ?></title>
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
<?php if ($cfg['style']) { ?>
    <style><?= is_array($cfg['style']) ? implode("\n", $cfg['style']) : $cfg['style'] ?></style>
<?php } ?>
</head>
<body>
<h1><?= $heading ?></h1>
<?php
if ($cfg['header']) {
    echo(getRow($cfg['header'], true));
    /*
    $row = '';
    if ($cfg['separator_on_start']) {
        $row .= $cfg['separator'];
    }
    foreach ($cfg['columns'] as $num => $col) {
        $header = $cfg['header'][$num];
        if ($col == 'name') {
            $row .= substr($header, 0, $maxFileLength) . str_repeat($cfg['filler'], $maxFileLength - mb_strlen($header));
        } elseif ($col == 'size') {
            $row .= str_repeat($cfg['filler'], $maxSizeLength - mb_strlen($header)) . $header;
        } elseif ($col == 'date') {
            $row .= $header . str_repeat($cfg['filler'], strlen($cfg['date_format']) - strlen($header));
        } else {
            throw new \InvalidArgumentException("Unknown column $col");
        }
        $row .= $cfg['separator'];
    }
    if (!$cfg['separator_on_end']) {
        $row = substr($row, 0, -strlen($cfg['separator']));
    }
    echo('<pre>' . $row . '</pre>');
    */
}
?>
<hr>
<pre>
<?php

function getRow(array $item, bool $isHeader = false) {
    global $cfg, $maxFileLength, $maxSizeLength, $relPath;
    $row = $type = '';
    if ($cfg['separator_on_start']) {
        $row .= $cfg['separator'];
    }
    foreach ($cfg['columns'] as $key => $col) {
        if ($col == 'name') {
            $value = $item[$key];
            $valueLength = mb_strlen($value);
            if (($lastDot = strrpos($value, '.')) !== false) {
                $ext = substr($value, $lastDot); // with leading '.'
                $extLower = strtolower($ext);
            } else {
                $ext = $extLower = '';
            }
            $base = basename($value, $ext);
            if ($valueLength <= $maxFileLength) {
                $name = $value;
            } else {
                $name = mb_substr($base, 0, $maxFileLength-mb_strlen($ext)-1) . '…' . $ext;
            }
            if (is_dir($value)) {
                $type = 'dir';
                $icon = 'folder';
            } elseif (in_array($extLower, ['.avi', '.flv', '.mkv', '.mp4', '.mpeg', '.mpg', '.ogv', '.ts', '.webm', '.wmv'])) {
                $type = 'video';
                $icon = 'video_file';
            } elseif (in_array($extLower, ['.jpg', '.svg', '.png', '.gif', '.tiff', '.bmp'])) {
                $type = 'image';
                $icon = 'photo'; // photo_album
            } elseif (in_array($extLower, ['.arj', '.bz2', '.gz', '.gzip', '.rar', '.tar', '.zip'])) {
                $type = 'archive';
                $icon = 'folder_zip';
            } else {
                $icon = 'insert_drive_file';
            }
            //echo('name: ' . $name . '; ext: ' . $ext);
            $nameHtml = "<a href=\"{$value}\"><span class=\"material-icons\">$icon</span> $name</a>" . str_repeat($cfg['filler'], $maxFileLength - mb_strlen($name));
            $row .= $nameHtml;
        } elseif ($col == 'size') {
            $row .= str_repeat($cfg['filler'], $maxSizeLength - mb_strlen($item[$key])) . $item[$key];
        } elseif ($col == 'date') {
            $row .= date($cfg['date_format'], isset($item[$key]) ? filemtime($item[$key]) : 0);
        } else {
            throw new \InvalidArgumentException("Unknown column $col");
        }
        $row .= $cfg['separator'];
    }
    if (!$cfg['separator_on_end']) {
        $row = substr($row, 0, -strlen($cfg['separator']));
    }
    if ($type == 'video') {
        $row .= $cfg['separator'] . "<a href=\"https://duoverso.com/watch?url={$relPath}/{$value}\"><span class=\"material-icons\">play_circle</span></a> ";
    }
    return ($isHeader ? '<pre>' . $row . '</pre>' : ($row . "\n"));
}

foreach ($items as $item) {
    echo(getRow($item));
}

// if ($_GET['debug'] ?? false) {
//     dump($cfg);
// }

?>
</pre>
<hr>
</body>
</html>

