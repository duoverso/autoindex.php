# autoindex.php

## NGINX configuration
```
location /files {
    try_files $uri $uri/ /files/index.php?$query_string;
    error_page 403 = /files/index.php?$query_string;
}
```

## autoindex.json
- **columns**: (default: `["name", "date", "size"]`)
- **date_format**: (default: `d-M-Y H:i`)
- **dir_suffix**: (default: `/`)
- **dirs_only**: (default: `false`)
- **exact_size**: Size in bytes or rounded in human readable units - power of 1024 (default: `true`)
- **filler**: String to fill gaps on start and end of columns (default: space ` `)
- **header**: Name columns in header, eg: `["Name", "Date", "Size"]` (default: `false`),
- **heading**: (default: `"<a href=\"/\">%host%</a> / %breadcrumbs%"`), see [Replacements](#replacements)
- **ignore**: (default: `["index.php", "autoindex.json"]`)
- **keep_parent_config**: (default: `true`)
- **link_to_parent**: (default: `true`)
- **max_file_length**: (default: `70`)
- **separator**: Columns separator (default: `    `)
- **separator_on_end**: (default: `false`)
- **separator_on_start**: (default: `false`)
- **show_hidden**: (default: `false`), recommended: `"ignore": ["."]` and `"link_to_parent": false`
- **size_precision**: Decimal points for `"exact_size": true` (default: `2`)
- **style**: (default: `false`), eg:
```
[
    "body { background: black; color: white; font-size: 14pt; }",
    "hr { border: 1px solid #333; }",
    "pre { line-height: 1.2em }",
    "a, a:link { color: deepskyblue; text-decoration: none }",
    "a:hover { color: skyblue }",
    "a:visited { color: magenta }",
    "a:visited:hover { color: violet }",
    "a:active { color: orange }"
]
```
- **title**: (default: `"%host%%path%"`), see [Replacements](#replacements)

### Replacements
- `%path%`
- `%breadcrumbs%`
- `%host%`
- `%port%`
